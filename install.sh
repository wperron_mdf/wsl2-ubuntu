# Update Package Manager
sudo apt-get update -y

# Install Python v2.X and v3.X
sudo apt install python python3 python-pip python3-pip -y

# Install Nodejs v11.X
sudo apt-get install curl software-properties-common -y
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install nodejs -y

# Install Typescript
npm install -g typescript

# Install Java 11 and maven
sudo apt-get update -y && sudo apt-get install java-common -y
wget https://d3pxv6yz143wms.cloudfront.net/11.0.3.7.1/java-11-amazon-corretto-jdk_11.0.3.7-1_amd64.deb
sudo dpkg --install java-11-amazon-corretto-jdk_11.0.3.7-1_amd64.deb
wget http://muug.ca/mirror/apache-dist/maven/maven-3/3.6.1/binaries/apache-maven-3.6.1-bin.tar.gz
sudo tar -xvzf apache-maven-3.6.1-bin.tar.gz
sudo apt install maven -y

# Install Rust from rustup
sudo curl https://sh.rustup.rs -sSf | sh

# Install aws cli and kubectl
pip3 install awscli --upgrade --user
curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl

# Install aws SDKs
pip3 install boto3
npm install -g aws-sdk

# install ruby & gem
sudo apt install -y ruby

# install Terraform
sudo apt install unzip -y
wget https://releases.hashicorp.com/terraform/0.12.5/terraform_0.12.5_linux_amd64.zip
sudo unzip terraform_0.12.5_linux_amd64.zip -d /usr/bin

# Docker CE
sudo apt-get remove docker docker-engine docker.io containerd runc -y
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo usermod -aG docker $(whoami)

